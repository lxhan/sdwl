package com.richbond.sdjdsm.util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by liwenxun on 15/7/28.
 */
public interface CropHandler {

    void onPhotoCropped(Uri uri);

    void onCropCancel();

    void onCropFailed(String message);

    CropParams getCropParams();

    Activity getContext();

    void goResult(Intent intent);
}
