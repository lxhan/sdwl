package com.richbond.sdjdsm.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.richbond.sdjdsm.R;

import java.util.List;

/**
 * Created by liwenxun on 15/8/12.
 */
public class ActivityUtil {

    /*
     * 鐣岄潰杩囧害鍔ㄧ敾锛屽紑濮�
     * */
    public static void pendingTransition_start(Activity context) {
        context.overridePendingTransition(R.anim.base_push_left_in,
                R.anim.base_push_left_out);
    }

    /*
     * 鐣岄潰杩囧害鍔ㄧ敾锛岀粨鏉�
     * */
    public static void pendingTransition_end(Activity context) {
        context.overridePendingTransition(R.anim.base_push_right_in,
                R.anim.base_push_right_out);
    }

    /*
     * 鎵撳紑涓�涓柊鐣岄潰
     * */
    public static void openActivity(Activity activity, Class<?> pClass) {
        openActivity(activity, pClass, null);
    }


    /*
     * 鎵撳紑涓�涓柊鐣岄潰
     * */
    public static void openActivity(Activity activity, Class<?> pClass,
                                    Bundle pBundle) {
        Intent intent = new Intent(activity, pClass);
        if (pBundle != null) {
            intent.putExtras(pBundle);
        }
        activity.startActivity(intent);
        pendingTransition_start(activity);
    }

    /*
     * 鎵撳紑涓�涓紝鏈夊洖璋冪殑鏂扮晫闈�
     * */
    public static void openActivityForResult(Activity activity,
                                             Class<?> pClass, Bundle pBundle, int requestCode) {
        Intent intent = new Intent(activity, pClass);
        if (pBundle != null) {
            intent.putExtras(pBundle);
        }
        activity.startActivityForResult(intent, requestCode);
        pendingTransition_start(activity);
    }

    /*
     * 鎵撳紑涓�涓柊鐣岄潰
     * */
    public static void openActivity(Activity activity, String pAction) {
        openActivity(activity, pAction, null);
    }

    /*
     * 鎵撳紑涓�涓柊鐣岄潰
     * */
    public static void openActivity(Activity activity, String pAction,
                                    Bundle pBundle) {
        Intent intent = new Intent(pAction);
        if (pBundle != null) {
            intent.putExtras(pBundle);
        }
        activity.startActivity(intent);
        pendingTransition_start(activity);
    }

    /*
     * 濞寸姴閰ｉ妴濠囨焾閵娧勭暠Activity閻℃帒锕ㄧ换鍐╃▔椤撱垺锛熼柣銊ュ閸ゆ垶绋夐悪婕渢ivity閻犲搫鐤囧ù鍡涘礆閺夎法淇洪梺顔哄妿濞堟垶绋夐敓浠嬪殝Activity闁挎稑濂旈懙鎴︽⒒鐎靛憡鐣盇ctivity闁煎浜滄慨鈺侇嚕閻熸澘姣�
     */
    public static void CloseActivity(Activity activity, Class<?> class1) {
        Intent intent = new Intent(activity, class1);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(intent);
    }

    /*
     * 閲嶅惎app
     * */
    public static void restart(Context f, Class<?> s) {
        Intent i = f.getPackageManager().getLaunchIntentForPackage(
                f.getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        f.startActivity(i);
        // Intent intent = new Intent(f, s);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
        // | Intent.FLAG_ACTIVITY_NEW_TASK);
        // f.startActivity(intent);
    }

    /**
     * get top activity from the stack
     * **/
    public static String getTopActivity(Activity context) {
        ActivityManager manager = (ActivityManager) context
                .getSystemService(context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);

        if (runningTaskInfos != null)
            return (runningTaskInfos.get(0).topActivity).toString();
        else
            return null;
    }


    public static void closeKeybord(Activity activity, EditText edit) {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit.getWindowToken(),0);
    }

}
