package com.richbond.sdjdsm.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Created by liwenxun on 15/8/17.
 */
/*
 * json相关处理　类
 * */
public class JsonUtil {


    public static <T> T getObjectFromString(JSONObject jsData,Class<T> classzz) throws Exception{
        T t = classzz.newInstance();
        Field[] fields = t.getClass().getDeclaredFields();
        for (Field field :fields){
            field.setAccessible(true);
            String valu ="";
            try{
                valu = String.valueOf(jsData.getString(field.getName()));
            }catch (JSONException e){
            }
            field.set(t, valu);
        }
        return t;
    }

    /*
     * 判断json 的返回状态
     * */
    public static boolean GetJsonStatu(JSONObject jsonObject) {
        try {
            if (jsonObject.getString("status").equalsIgnoreCase("ok")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            return false;
        }
    }

    /*
     * 判断json 的返回状态
     * */
    public static boolean GetJsonStatu(JSONArray jsonArray) {
        try {
            if (jsonArray.getJSONObject(1).getJSONObject("status")
                    .getString("mark").equals("ok")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            return false;
        }
    }

    public static String GetJsonString(String val) {
        String reString = "";
        if (val.toLowerCase().equals("null"))
            return reString;
        return val;
    }

    public static Boolean GetBoolean(JSONObject jsonObject, String key) {
        try {
            return jsonObject.getBoolean(key);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            return false;
        } catch (NullPointerException e) {
            // TODO: handle exception
            return false;
        }
    }

    public static JSONArray GetArr(JSONObject jsonObject, String key) {
        try {
            return jsonObject.getJSONArray(key);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            return null;
        } catch (NullPointerException e) {
            // TODO: handle exception
            return null;
        }
    }

    public static String GetString(JSONObject jsonObject, String key) {
        try {
            return GetJsonString(jsonObject.getString(key));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            return "";
        } catch (NullPointerException e) {
            // TODO: handle exception
            return "";
        }
    }

    public static String GetString(Map<String, String> map, String key) {
        try {
            return GetJsonString(map.get(key));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            return "";
        }
    }

    public static Date GetDateDefault(JSONObject jsonObject, String key,
                                      String type, Locale locale) {
        try {
            String dateString = GetString(jsonObject, key);
            if (dateString == null || dateString.trim().equals("")
                    || dateString.trim().equals("null"))
                return null;
            DateFormat df = new SimpleDateFormat(type, locale);
            Date date = new Date();
            try {
                date = df.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block\
                Log.e("StringToDate", dateString + "    " + e);
                e.printStackTrace();
            }
            return date;
        } catch (Exception e) {
            // TODO: handle exception
            return null;
        }
    }

    public static JSONObject GetJsonObject(JSONObject jsonObject, String key) {
        try {
            return new JSONObject(GetString(jsonObject, key));
        } catch (Exception e) {
            // TODO: handle exception
            return null;
        }
    }

    // 锟斤拷锟叫伙拷锟斤拷为锟街凤拷
    public static String object2String(Object obj) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                    byteArrayOutputStream);
            objectOutputStream.writeObject(obj);
            String serStr = byteArrayOutputStream.toString("ISO-8859-1");
            serStr = java.net.URLEncoder.encode(serStr, "UTF-8");

            objectOutputStream.close();
            byteArrayOutputStream.close();
            return serStr;
        } catch (Exception e) {
            // TODO: handle exception
            return "";
        }

    }

    // 锟斤拷锟斤拷锟叫伙拷锟街凤拷为锟斤拷锟斤拷
    @SuppressWarnings("unchecked")
    public static Object getObjectFromString(String serStr) {
        try {
            String redStr = java.net.URLDecoder.decode(serStr, "UTF-8");
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                    redStr.getBytes("ISO-8859-1"));
            ObjectInputStream objectInputStream = new ObjectInputStream(
                    byteArrayInputStream);
            Object object = objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            return object;
        } catch (Exception e) {
            // TODO: handle exception
            return null;
        }

    }

}
