package com.richbond.sdjdsm.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.richbond.sdjdsm.R;
import com.richbond.sdjdsm.dao.GoodsTypeDao;

import java.util.Arrays;
import java.util.List;

/**
 * Created by LuHan on 2015/12/31.
 */
public class DBHelper extends SQLiteOpenHelper {
    private final static String DB_NAME = "sdjdsm.db";//数据库名
    private final static int VERSION = 1;//版本号
    //表名
    public final static String TABLE_CUSTOM = "custom";//表名：寄件人信息
    public final static String TABLE_EMS = "ems";//表名：物流信息
    public final static String TABLE_GOODS_TYPE = "goods_type";//表名：货物类型
    //建表语句
    private final static String CREATE_TABLE_CUSTOM = "create table IF NOT EXISTS " + TABLE_CUSTOM + "(" +
            "id integer primary key autoincrement," +//寄件人信息
            "phone varchar(20)," +//寄件人手机号码
            "name varchar(20)," +//寄件人姓名
            "idcard varchar(20))";//寄件人身份证号码
    private final static String CREATE_TABLE_EMS = "create table IF NOT EXISTS " + TABLE_EMS + "(" +
            "id integer primary key autoincrement," +//物流单信息
            "status varchar(20)," +//提交状态（yes/no）
            "userid varchar(20)," +//快递人员id
            "phone varchar(20)," +//快递人员手机号码
            "barcode varchar(20)," +//物流单号
            "customphone varchar(20)," +//寄件人手机号码
            "customname varchar(20)," +//寄件人姓名
            "hwlx varchar(20)," +//货物类型名称
            "hwlxbm varchar(20)," +//货物类型编码
            "rphone varchar(20)," +//收件人手机号码
            "latitude varchar(20)," +//纬度
            "longitude varchar(20)," +//经度
            "appversion varchar(20)," +//当前app版本
            "systemversion varchar(20)," +//快递人员手机安卓版本
            "province varchar(20)," +//省
            "city varchar(20)," +//市
            "county varchar(20)," +//区、县
            "address varchar(20)," +//详细地址
            "iskxys varchar(2)," +//是否开箱验视
            "iswxp varchar(2)," +//是否危险品
            "iswjp varchar(2)," +//是否违禁品
            "gzlx varchar(20)," +//贵重类型
            "systemname varchar(20)," +//系统名称
            "web varchar(20)," +//web
            "photo_a longtext," +//照片
            "photo_b longtext," +//照片
            "photo_c longtext," +//照片
            "customcard varchar(20))";//寄件人身份证号码
    private final static String CREATE_TABLE_GOODS_TYPE = "create table IF NOT EXISTS " + TABLE_GOODS_TYPE + "(" +
            "id integer primary key autoincrement," +//货物类型
            "name varchar(20))";
    private Context context;

    //自带的构造方法
    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                    int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    //为了每次构造时不用传入dbName和版本号，自己得新定义一个构造方法
    public DBHelper(Context cxt) {
        this(cxt, DB_NAME, null, VERSION);//调用上面的构造方法
    }

    //版本变更时
    public DBHelper(Context cxt, int version) {
        this(cxt, DB_NAME, null, version);
    }

    //当数据库创建的时候调用
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CUSTOM);
        db.execSQL(CREATE_TABLE_EMS);
        db.execSQL(CREATE_TABLE_GOODS_TYPE);
        db.execSQL(GoodsTypeDao.INIT_DATA);//初始化货物类型数据

        /**
         * 获取初始化的数据，并且直接插入数据库中。
         */
        String[] goods_type = this.context.getResources().getStringArray(R.array.goods_type);
        List<String> list = Arrays.asList(goods_type);
        for (int i = 0; i < list.size(); i++) {
            db.execSQL("INSERT INTO goods_type(name) VALUES('" + list.get(i) + "');");
        }
    }

    //版本更新时调用
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        for (int i = oldVersion; i <= newVersion; i++) {
            switch (i) {
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:

                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    break;
                case 9:
                    break;
                default:
                    break;
            }
        }

    }
}
