package com.richbond.sdjdsm.util;


import android.util.Log;

import com.richbond.sdjdsm.bean.BaseBean;
import com.richbond.sdjdsm.net.RequestParams;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class BeanToParams {
    private static final String TAG = BeanToParams.class.getSimpleName();

    /**
     * 传入实体类，返回请求参数
     *
     * @param baseBean
     * @return
     */
    public static RequestParams getRequestParams(BaseBean baseBean) {
        RequestParams params = new RequestParams();
        try {
            Class classzz = baseBean.getClass();
            List<Field> list = new ArrayList<Field>();
            if (classzz.getSuperclass() != Object.class) {
                Field[] parentFields = classzz.getSuperclass()
                        .getDeclaredFields();
                for (Field parentField : parentFields) {
                    list.add(parentField);
                }
            }
            Field[] fields = classzz.getDeclaredFields();
            for (Field field : fields) {
                list.add(field);
            }
            for (Field f : list) {
                f.setAccessible(true);
                Log.v(TAG, f.getName() + ":" + String.valueOf(f.get(baseBean)));
                params.put(f.getName(), String.valueOf(f.get(baseBean)));
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return params;
    }
}
