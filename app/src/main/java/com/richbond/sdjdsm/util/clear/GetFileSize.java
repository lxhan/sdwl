package com.richbond.sdjdsm.util.clear;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;

/**
 * Created by sunfish on 2015/10/15.
 */
public class GetFileSize {
    /**
     * 取得文件大小
     * @param file
     * @return
     * @throws Exception
     */
    public long getFileSizes(File file) throws Exception{
        long s=0;
        if (file.exists()) {
            FileInputStream fis = null;
            fis = new FileInputStream(file);
            s= fis.available();
        } else {
            file.createNewFile();
            System.out.println("文件不存在");
        }
        return s;
    }
    /**
     * 取得文件夹大小 // 递归
     * @param f
     * @return
     * @throws Exception
     */
    public long getFileSize(File f)throws Exception
    {
        long size = 0;
        File flist[] = f.listFiles();
        for (int i = 0; i < flist.length; i++)
        {
            if (flist[i].isDirectory()) {
                size = size + getFileSize(flist[i]);
            } else {
                size = size + flist[i].length();
            }
        }
        return size;
    }

    /**
     * //转换文件大小
     * @param fileS
     * @return
     */
    public String FormetFileSize(long fileS) {
        DecimalFormat df = new DecimalFormat("0.00");
        String fileSizeString = "";
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "K";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "M";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "G";
        }
        return fileSizeString;
    }

    /**
     * 递归求取目录文件个数
     * @param f
     * @return
     */
    public long getlist(File f){
        long size = 0;
        File flist[] = f.listFiles();
        size=flist.length;
        for (int i = 0; i < flist.length; i++) {
            if (flist[i].isDirectory()) {
                size = size + getlist(flist[i]);
                size--;
            }
        }
        return size;
    }

    //传入路径 获取文件或者文件夹大小
    public long getCacheSize(File file){
        GetFileSize g = new GetFileSize();
        long l = 0;
        try {
            if (file.isDirectory()){
                l = g.getFileSize(file);
            }else{
                l = g.getFileSizes(file);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return l;
    }
    /*public static void main(String args[])
    {
        GetFileSize g = new GetFileSize();
        long startTime = System.currentTimeMillis();
        try
        {
            long l = 0;
            String path = "C:\\WINDOWS";
            File ff = new File(path);
            if (ff.isDirectory()) { //如果路径是文件夹的时候
                System.out.println("文件个数           " + g.getlist(ff));
                System.out.println("目录");
                l = g.getFileSize(ff);
                System.out.println(path + "目录的大小为：" + g.FormetFileSize(l));
            } else {
                System.out.println("     文件个数           1");
                System.out.println("文件");
                l = g.getFileSizes(ff);
                System.out.println(path + "文件的大小为：" + g.FormetFileSize(l));
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        System.out.println("总共花费时间为：" + (endTime - startTime) + "毫秒...");
    }*/
}
