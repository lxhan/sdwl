package com.richbond.sdjdsm.util;

import android.app.Activity;
import android.content.Context;
import android.view.WindowManager;

/**
 * Created by liwenxun on 15/8/12.
 */
public class DensityHelper {
    /**
     * 閿熸枻鎷烽敓鏂ゆ嫹鍙敓渚ュ垎鎲嬫嫹閿熺粸杈炬嫹 dp 閿熶茎纰夋嫹浣� 杞敓鏂ゆ嫹涓� px(閿熸枻鎷烽敓鏂ゆ嫹)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 閿熸枻鎷烽敓鏂ゆ嫹鍙敓渚ュ垎鎲嬫嫹閿熺粸杈炬嫹 px(閿熸枻鎷烽敓鏂ゆ嫹) 閿熶茎纰夋嫹浣� 杞敓鏂ゆ嫹涓� dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }
    /**
     * 閿熸枻鎷烽敓鏂ゆ嫹鍙敓渚ュ垎鎲嬫嫹閿熺粸鍖℃嫹閿燂拷
     */
    public static int getScreenWidth(Context context)
    {
        return ((Activity)context).getWindowManager().getDefaultDisplay().getWidth();
    }

    /**
     * 閿熸枻鎷烽敓鏂ゆ嫹鍙敓渚ュ垎鎲嬫嫹閿熺粸楂樿鎷�
     */
    public static int getScreenHeight(Context context)
    {
        return ((Activity)context).getWindowManager().getDefaultDisplay().getHeight();
    }



    /**
     * 閿熸枻鎷峰彇閿熸枻鎷峰箷閿熶茎灏鸿揪鎷�
     * @param context
     * @return
     */
    public static int[] getScreenSize(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(
                Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();//閿熸枻鎷峰箷閿熸枻鎷烽敓锟�
        int height = wm.getDefaultDisplay().getHeight();//閿熸枻鎷峰箷閿熺璁规嫹
        int[] size = {width,height};

        return size;
    }

    /**
     * 閿熸枻鎷峰彇鐘舵�侀敓鏂ゆ嫹閿熺璁规嫹
     * @param context
     * @return
     */
    public static int getStatusBarHeight(Context context){
        int statusBarHeight = 0;
        // 閿熸枻鎷烽敓鏂ゆ嫹鍙敓鏂ゆ嫹瑕侀敓鏂ゆ嫹鍙栭敓鏂ゆ嫹骞曢敓绔鎷�
        int screenHeight = getScreenSize(context)[1];

        switch(screenHeight){
            case 240:
                statusBarHeight = 20;
                break;
            case 480:
                statusBarHeight = 25;
                break;
            case 800:
                statusBarHeight = 38;
                break;
            default:
                break;
        }

        return statusBarHeight;
    }
}
