package com.richbond.sdjdsm.util;

import android.util.Log;

import com.richbond.sdjdsm.entity.CityModel;
import com.richbond.sdjdsm.entity.DistrictModel;
import com.richbond.sdjdsm.entity.ProvinceModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 地区处理类
 * Created by Administrator on 2016/4/18.
 */
public class RegionHandler {
    private static final String TAG = RegionHandler.class.getSimpleName();
    /**
     * 存储所有的解析对象
     */
    private List<ProvinceModel> provinceList = new ArrayList<ProvinceModel>();

    public RegionHandler() {

    }

    public List<ProvinceModel> getDataList() {
        return provinceList;
    }

    public ProvinceModel provinceModel = new ProvinceModel();
    public CityModel cityModel = new CityModel();
    public DistrictModel districtModel = new DistrictModel();

    public void initData(String content) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(content);
            JSONObject jsObjData = jsonObject.getJSONObject("data");
            JSONArray jsArrayProvince = jsObjData.getJSONArray("area-1-0");
            for (int i = 0; i < jsArrayProvince.length(); i++) {
                JSONObject jsObjectProvince = jsArrayProvince.getJSONObject(i);
                provinceModel = new ProvinceModel();
                provinceModel.setName(jsObjectProvince.getString("name"));
                provinceModel.setRegion_id(jsObjectProvince.getString("region_id"));
                provinceModel.setCityList(new ArrayList<CityModel>());
                Log.v(TAG, "省份：" + jsObjectProvince.getString("name").toString());
                JSONArray jsArrayCity = jsObjData.getJSONArray("area-2-" + jsObjectProvince.getString("region_id"));
                for (int j = 0; j < jsArrayCity.length(); j++) {
                    JSONObject jsObjectCity = jsArrayCity.getJSONObject(j);
                    cityModel = new CityModel();
                    cityModel.setName(jsObjectCity.getString("name"));
                    cityModel.setRegion_id(jsObjectCity.getString("region_id"));
                    cityModel.setDistrictList(new ArrayList<DistrictModel>());
                    Log.v(TAG, "城市：" + jsObjectCity.getString("name").toString());
                    JSONArray jsArrayDistrict = null;
                    try {
                        jsArrayDistrict = jsObjData.getJSONArray("area-3-" + jsObjectCity.getString("region_id"));
                        if (null != jsArrayDistrict) {
                            for (int k = 0; k < jsArrayDistrict.length(); k++) {
                                JSONObject jsObjectDistrict = jsArrayDistrict.getJSONObject(k);
                                districtModel = new DistrictModel();
                                districtModel.setName(jsObjectDistrict.getString("name"));
                                districtModel.setRegion_id(jsObjectDistrict.getString("region_id"));
                                Log.v(TAG, "县区：" + jsObjectDistrict.getString("name").toString());
                                cityModel.getDistrictList().add(districtModel);//将区县的数据加入到他的城市中去
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.v(TAG, jsObjectCity.getString("name").toString() + "没有区县");
                        districtModel = new DistrictModel();
                        districtModel.setName("");
                        districtModel.setRegion_id("");
                        cityModel.getDistrictList().add(districtModel);//将区县的数据加入到他的城市中去
                    }
                    provinceModel.getCityList().add(cityModel);//将城市的数据加入到他的省份中去
                }
                provinceList.add(provinceModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
