package com.richbond.sdjdsm;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.richbond.sdjdsm.bean.OrderBean;
import com.richbond.sdjdsm.bean.UpdateBean;
import com.richbond.sdjdsm.common.BaseActivity;
import com.richbond.sdjdsm.dao.OrderDao;
import com.richbond.sdjdsm.util.DialogHelper;
import com.richbond.sdjdsm.util.JsonUtil;
import com.richbond.sdjdsm.util.StringUtils;
import com.richbond.sdjdsm.util.UIHelp;
import com.richbond.sdjdsm.util.update.UpdateManager;
import com.richbond.sdjdsm.webserver.PoiWS;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by liwenxun on 15/9/6.
 */
public class NotSubmittedInfoActivity extends BaseActivity {

    private static final String TAG = NotSubmittedInfoActivity.class.getSimpleName();
    @Bind(R.id.tvTxm)
    TextView tvTxm;
    @Bind(R.id.tvSjhm)
    TextView tvSjhm;
    @Bind(R.id.tvXm)
    TextView tvXm;
    @Bind(R.id.tvSfz)
    TextView tvSfz;
    @Bind(R.id.tvNj)
    TextView tvNj;
    @Bind(R.id.tvSjrhm)
    TextView tvSjrhm;
    private PoiWS ws;

    private String barcode = "";

    public OrderDao orderDao = new OrderDao(this);
    private boolean isTask = false;
    private OrderBean orderBean;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setBaseContentLayout(R.layout.activity_submitinfo);
        ButterKnife.bind(this);
        init();
        ws = new PoiWS(this);
    }

    private void init() {
        setTitle("物流单详情");
        setRightBtn("提交", R.color.transparent, 80, 50);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            barcode = bundle.getString("barcode");
            orderBean = orderDao.findDataByBarcode(barcode).get(0);
        } else {
            orderBean = new OrderBean();
        }
        tvSfz.setText(StringUtils.getHideCharacter(orderBean.getCustomcard()));//寄件人身份证号码
        tvSjhm.setText(StringUtils.getHideCharacter(orderBean.getCustomphone()));//寄件人手机号码
        tvXm.setText(StringUtils.getHideCharacter(orderBean.getCustomname()));//寄件人姓名
        tvSjrhm.setText(StringUtils.getHideCharacter(orderBean.getRphone()));//收件人手机号码
        tvTxm.setText(barcode);//物流单号
        tvNj.setText(orderBean.getHwlx());//货物类型
    }

    @Override
    protected void onRightClick(View v) {
        if (UIHelp.isNetworkConnected(this)) {
            if (isTask) {
                return;
            }
            isTask = true;
            new UpdateTask().execute();
        } else {
            DialogHelper.showToast(NotSubmittedInfoActivity.this, "没有网络，无法提交！");
        }
    }

    /**
     * 获取更新的异步任务
     */
    private class UpdateTask extends AsyncTask<String, String, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            return ws.Update();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            Log.v("获取更新的json---result=======", String.valueOf(result));
            if (JsonUtil.GetJsonStatu(result)) {
                final UpdateBean updateBean = new UpdateBean();
                try {
                    JSONObject jsonObject = result.getJSONObject("data");
                    updateBean.setAppVersionCode(jsonObject.getString("appVersionCode"));
                    updateBean.setAppVersionName(jsonObject.getString("appVersionName"));
                    updateBean.setAppDownloadUrl(jsonObject.getString("appDownloadUrl"));
                    updateBean.setWebInterface(jsonObject.getString("webInterface"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                UpdateManager manager = new UpdateManager(NotSubmittedInfoActivity.this, updateBean);
                if (manager.isUpdate()) {
                    DialogHelper.closeProgressDialog();
                    // 显示提示对话框
                    DialogHelper.showToast(NotSubmittedInfoActivity.this, "请在设置中更新到最新版本,否则无法使用!");
                    isTask = false;
                } else {
                    new SendInfo().execute(orderBean);
                }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            DialogHelper.showProgressDialog(NotSubmittedInfoActivity.this, "正在提交...");
        }
    }

    class SendInfo extends AsyncTask<OrderBean, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(OrderBean... params) {
            return ws.sendInfo(params[0]);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            Log.i("SendInfo=======", String.valueOf(result));
            isTask = false;
            if (JsonUtil.GetJsonStatu(result)) {
                orderDao.updateStatus(barcode, OrderDao.STATUS_YES);
                DialogHelper.showToast(NotSubmittedInfoActivity.this, "已提交成功！");
                NotSubmittedInfoActivity.this.finish();
            } else {
                DialogHelper.showToast(NotSubmittedInfoActivity.this, JsonUtil.GetString(result, "msg"));
            }
            DialogHelper.closeProgressDialog();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            DialogHelper.showProgressDialog(NotSubmittedInfoActivity.this, "正在提交...");
        }
    }
}
