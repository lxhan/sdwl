package com.richbond.sdjdsm.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 *
 */
public class SharedPreferencesUtil {
	public final static String COOKIE_KEY = "Access_Cookie";
	public final static String USER_AGENT_KEY = "Access_UserAgent";
	public final static String VERSION_KEY = "App_Version";

	/**
	 * 获取共享偏好操作实例
	 * @param context
	 * @return
	 */
	private static SharedPreferences getSharedPreferences(Context context){
		return context.getSharedPreferences("", Context.MODE_PRIVATE);
	}
	/**
	 * 获取Editor对象
	 * @param context
	 * @return
	 */
	private static Editor getEditor(Context context){
		return getSharedPreferences(context).edit();
	}
	/**
	 * 根据key获取boolean值
	 * @param context
	 * @param key
	 * @return
	 */
	public static boolean getBoolean(Context context,String key){
		return getSharedPreferences(context).getBoolean(key, false);
	}
	/**
	 * 设置key的boolean值
	 * @param context
	 * @param key
	 * @param flag
	 */
	public static void setBoolean(Context context,String key,boolean flag){
		Editor editor = getEditor(context);
		editor.putBoolean(key,flag);
		editor.commit();
	}
	/**
	 * 设置key的int值
	 * @param context
	 * @param key
	 * @param value
	 */
	public static void setInt(Context context,String key,int value){
		Editor editor = getEditor(context);
		editor.putInt(key, value);
		editor.commit();
	}
	
	/**
	 * 根据key获取int值
	 * @param context
	 * @param key
	 * @return
	 */
	public static int getInt(Context context,String key){
		return getSharedPreferences(context).getInt(key, 0);
	}
	/**
	 * 设置key的long值
	 * @param context
	 * @param key
	 * @param value
	 */
	public static void setLong(Context context,String key,long value){
		Editor editor = getEditor(context);
		editor.putLong(key, value);
		editor.commit();
	}
	
	/**
	 * 根据key获取long值
	 * @param context
	 * @param key
	 * @return
	 */
	public static long getLong(Context context,String key){
		return getSharedPreferences(context).getLong(key, 0);
	}
	/**
	 * 设置key的String值
	 * @param context
	 * @param key
	 * @param value
	 */
	public static void setString(Context context,String key,String value){
		Editor editor = getEditor(context);
		editor.putString(key, value);
		editor.commit();
	}
	/**
	 * 根据key获取String值
	 * @param context
	 * @param key
	 * @return
	 */
	public static String getString(Context context,String key){
		return getSharedPreferences(context).getString(key, null);
	}
	
	
}
