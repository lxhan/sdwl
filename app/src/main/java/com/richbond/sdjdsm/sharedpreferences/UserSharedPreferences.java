package com.richbond.sdjdsm.sharedpreferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.richbond.sdjdsm.bean.UserBean;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * SharedPreferences操作数据存储的工具类（用户信息）
 * Created by Administrator on 2016/4/14.
 */
public class UserSharedPreferences {
    private static final String FILE_NAME = "userInfoData";
    private static final String KEY = "userInfoData";
    private static final Gson GSON = new Gson();
    private static final String TAG = UserSharedPreferences.class.getSimpleName();
    private static SharedPreferences sharedPreferences;

    /**
     * @param context
     * @return
     */
    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(FILE_NAME, Activity.MODE_PRIVATE);
    }

    /**
     * 添加用户
     *
     * @param bean
     * @param context
     */
    public static void add(UserBean bean, Context context) {
        String userJson = GSON.toJson(bean);
        sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY, userJson);
        editor.commit();
    }

    /**
     * 更新用户
     *
     * @param bean
     * @param context
     */
    public static void update(UserBean bean, Context context) {
        String userJson = GSON.toJson(bean);
        sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY, userJson);
        editor.commit();
    }

    /**
     * 删除用户
     *
     * @param context
     */
    public static void delete(Context context) {
        sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY, "");
        editor.commit();
    }

    /**
     * 查询用户
     *
     * @param context
     */
    public static UserBean find(Context context) {
        String userJson = getSharedPreferences(context).getString(KEY, "");//没有找到默认为空
        if ("".equals(userJson)) {
            UserBean bean = new UserBean();
            bean.setIsLogin("0");
            add(bean, context);
            return bean;
        } else {
            return GSON.fromJson(userJson, UserBean.class);
        }
    }

    /**
     * 登录的方法 -> 修改 isLogin的值为 1
     *
     * @param context
     */
    public static void login(Context context, JSONObject jsonObject) throws JSONException {
        jsonObject.put("isLogin", "1");
        String userInfoData = jsonObject.toString();
        sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY, userInfoData);
        editor.commit();
    }

    /**
     * 注销的方法 -> 修改 isLogin的值为 0
     *
     * @param context
     */
    public static void logout(Context context) {
        sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        UserBean bean = find(context);
        bean.setIsLogin("0");
        editor.putString(KEY, GSON.toJson(bean));
        editor.commit();
    }
}
