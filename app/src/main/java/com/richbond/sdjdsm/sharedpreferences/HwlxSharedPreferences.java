package com.richbond.sdjdsm.sharedpreferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.richbond.sdjdsm.bean.HwlxBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * SharedPreferences操作数据存储的工具类(内件名称)
 * Created by Administrator on 2016/4/14.
 */
public class HwlxSharedPreferences {
    private static final String FILE_NAME = "hwlxData";
    private static final String KEY = "hwlx";
    private static final Gson GSON = new Gson();
    private static final String TAG = HwlxSharedPreferences.class.getSimpleName();
    private static SharedPreferences sharedPreferences;

    /**
     * @param context
     * @return
     */
    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(FILE_NAME, Activity.MODE_PRIVATE);
    }

    /**
     * 添加
     *
     * @param jsonArray
     * @param context
     */
    public static void add(String jsonArray, Context context) {
        sharedPreferences = getSharedPreferences(context);
        if (null != sharedPreferences){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEY, jsonArray);
            editor.commit();
        }
    }

    /**
     * 更新
     *
     * @param bean
     * @param context
     */
    public static void update(HwlxBean bean, Context context) {
        String userJson = GSON.toJson(bean);
        sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY, userJson);
        editor.commit();
    }

    /**
     * 删除
     *
     * @param context
     */
    public static void delete(Context context) {
        sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY, "");
        editor.commit();
    }

    /**
     * 查询
     *
     * @param context
     */
    public static List<HwlxBean> find(Context context) {
        String hwlx = getSharedPreferences(context).getString(KEY, "");
        List<HwlxBean> list = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(hwlx);
            for (int i = 0 ; i<jsonArray.length();i++){
                HwlxBean hwlxBean = new HwlxBean();
                JSONObject res = (JSONObject) jsonArray.get(i);
                hwlxBean.setDm(res.getString("dm"));
                hwlxBean.setMc(res.getString("mc"));
                hwlxBean.setId(res.getString("id"));
                hwlxBean.setIsvalid(res.getString("isvalid"));
                hwlxBean.setPreant_dm(res.getString("preant_dm"));
                list.add(hwlxBean);
            }
            return list;
        } catch (JSONException e) {
            e.printStackTrace();
            return list;
        }
    }

}
