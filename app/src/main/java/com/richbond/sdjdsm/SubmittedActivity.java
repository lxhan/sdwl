package com.richbond.sdjdsm;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.richbond.sdjdsm.bean.OrderBean;
import com.richbond.sdjdsm.common.BaseActivity;
import com.richbond.sdjdsm.dao.OrderDao;
import com.richbond.sdjdsm.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by liwenxun on 15/9/6.
 */
public class SubmittedActivity extends BaseActivity {
    @Bind(R.id.btn_cancel)
    Button btn_cancel;
    @Bind(R.id.tv_selected)
    TextView tv_selected;
    @Bind(R.id.btn_delete)
    Button btn_delete;
    @Bind(R.id.ll_delete)
    LinearLayout ll_delete;
    @Bind(R.id.listView)
    ListView listView;
    @Bind(R.id.tv_tips)
    TextView tv_tips;
    private LayoutInflater inflater;
    private PropagandaAdapter adapter;
    private boolean isMulChoice = false; //是否多选
    public OrderDao orderDao = new OrderDao(this);
    private List<OrderBean> list = null;
    private List<OrderBean> selectid = new ArrayList<OrderBean>();

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setBaseContentLayout(R.layout.activity_propaganda);
        ButterKnife.bind(this);
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != list) {
            list.clear();
        }
        list = orderDao.findData(OrderDao.STATUS_YES);
        tv_tips.setText(list.size() > 0 ? list.size() + "" : "0");
        adapter.notifyDataSetChanged();
    }


    private void init() {
        setTitle("我的物流单");
        ll_delete.setVisibility(View.GONE);
        list = orderDao.findData(OrderDao.STATUS_YES);
        tv_tips.setText(list.size() > 0 ? list.size() + "" : "0");
        adapter = new PropagandaAdapter(this, tv_selected);
        listView.setAdapter(adapter);
        inflater = LayoutInflater.from(this);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                return false;
            }
        });
    }

    @OnClick({R.id.btn_cancel, R.id.btn_delete})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                isMulChoice = false;
                selectid.clear();
                adapter = new PropagandaAdapter(this, tv_selected);
                listView.setAdapter(adapter);
                ll_delete.setVisibility(View.INVISIBLE);
                break;
            case R.id.btn_delete:
                isMulChoice = false;
                for (int i = 0; i < selectid.size(); i++) {
                    if (orderDao.deleteByBarcode(selectid.get(i).getBarcode())) {
                        list.remove(i);
                    }
                }
                selectid.clear();
                adapter = new PropagandaAdapter(this, tv_selected);
                listView.setAdapter(adapter);
                ll_delete.setVisibility(View.INVISIBLE);
                onResume();
                break;
        }
    }

    class PropagandaAdapter extends BaseAdapter {
        private Context context;
        private HashMap<Integer, View> mView;
        public HashMap<Integer, Integer> visiblecheck;//用来记录是否显示checkBox
        public HashMap<Integer, Boolean> ischeck;
        private TextView txtcount;

        public PropagandaAdapter(Context context, TextView txtcount) {
            super();
            this.context = context;
            this.txtcount = txtcount;
            mView = new HashMap<Integer, View>();
            visiblecheck = new HashMap<Integer, Integer>();
            ischeck = new HashMap<Integer, Boolean>();
            if (isMulChoice) {
                for (int i = 0; i < list.size(); i++) {
                    ischeck.put(i, false);
                    visiblecheck.put(i, CheckBox.VISIBLE);
                }
            } else {
                for (int i = 0; i < list.size(); i++) {
                    ischeck.put(i, false);
                    visiblecheck.put(i, CheckBox.INVISIBLE);
                }
            }
        }

        @Override
        public int getCount() {

            if (list.size() > 0) {
                return list.size();
            } else {
                return 0;
            }
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = mView.get(position);
            if (view == null) {
                view = inflater.inflate(R.layout.item_submitted, null);
                TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
                TextView tv_customer_phone = (TextView) view.findViewById(R.id.tv_customer_phone);
                TextView tv_barcode = (TextView) view.findViewById(R.id.tv_barcode);
                final View view_selected = (View) view.findViewById(R.id.view_selected);
                final CheckBox ceb = (CheckBox) view.findViewById(R.id.checkbox_delete);

                ceb.setChecked(ischeck.get(position));
                ceb.setVisibility(visiblecheck.get(position));

                tv_name.setText("客户姓名：" + StringUtils.getHideCharacter(list.get(position).getCustomname()));
                tv_customer_phone.setText("客户手机：" + StringUtils.getHideCharacter(list.get(position).getCustomphone()));
                tv_barcode.setText("物流单号：" + list.get(position).getBarcode());

                view.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        isMulChoice = true;
                        selectid.clear();
                        ll_delete.setVisibility(View.VISIBLE);
                        for (int i = 0; i < list.size(); i++) {
                            adapter.visiblecheck.put(i, CheckBox.VISIBLE);
                        }
                        adapter = new PropagandaAdapter(context, txtcount);
                        listView.setAdapter(adapter);
                        return true;
                    }
                });
                view.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        if (isMulChoice) {
                            if (ceb.isChecked()) {
                                ceb.setChecked(false);
                                view_selected.setVisibility(View.GONE);
                                selectid.remove(list.get(position));
                            } else {
                                ceb.setChecked(true);
                                view_selected.setVisibility(View.VISIBLE);
                                selectid.add(list.get(position));
                            }
                            txtcount.setText("共选择了" + selectid.size() + "项");
                        } else {
                            goToContent(position);
                        }
                    }
                });
                mView.put(position, view);
            }
            return view;
        }
    }

    public void goToContent(int tag) {
        Bundle bundle = new Bundle();
        bundle.putString("barcode", list.get(tag).getBarcode());
        openActivity(SubmittedInfoActivity.class, bundle);
    }
}
