package com.richbond.sdjdsm.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.richbond.sdjdsm.bean.CustomBean;
import com.richbond.sdjdsm.util.DBHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * 寄件人信息增删改查（寄件人手机号码，寄件人姓名，寄件人身份证号码）
 */
public class CustomDao {
    private String table_name = DBHelper.TABLE_CUSTOM;//表名
    private static final String TAG = CustomDao.class.getSimpleName();
    DBHelper helper = null;

    public CustomDao(Context cxt) {
        helper = new DBHelper(cxt);
    }

    /**
     * 当Activity中调用此构造方法，传入一个版本号时，系统会在下一次调用数据库时调用Helper中的onUpgrade()方法进行更新
     *
     * @param cxt
     * @param version
     */
    public CustomDao(Context cxt, int version) {
        helper = new DBHelper(cxt, version);
    }

    // 插入操作
    public boolean insertData(CustomBean bean) {
        long isSucceed;
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name", bean.getName());
        values.put("phone", bean.getPhone());
        values.put("idcard", bean.getIdcard());
        isSucceed = db.insert(table_name, null, values);
        db.close();
        return isSucceed > 0;
    }

    // 查询操作
    public List<CustomBean> findData() {
        List<CustomBean> list = new ArrayList<CustomBean>();
        String sql = "select * from " + table_name;
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            CustomBean bean = new CustomBean();
            //String name = cursor.getString(0); //获取第一列的值,第一列的索引从0开始
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String phone = cursor.getString(cursor.getColumnIndex("phone"));
            String idcard = cursor.getString(cursor.getColumnIndex("idcard"));

            bean.setId(id);
            bean.setName(name);
            bean.setPhone(phone);
            bean.setIdcard(idcard);

            list.add(bean);
            Log.v(TAG, "我是" + list.size() + bean.toString());
        }
        cursor.close();
        db.close();
        return list;
    }

    // 查询操作
    public List<CustomBean> findDataByWord(String keyWord) {
        List<CustomBean> list = new ArrayList<CustomBean>();
        String sql = "select * from " + table_name + " where (phone like '%" + keyWord + "%')";
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            CustomBean bean = new CustomBean();
            //String name = cursor.getString(0); //获取第一列的值,第一列的索引从0开始
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String phone = cursor.getString(cursor.getColumnIndex("phone"));
            String idcard = cursor.getString(cursor.getColumnIndex("idcard"));

            bean.setId(id);
            bean.setName(name);
            bean.setPhone(phone);
            bean.setIdcard(idcard);
            list.add(bean);
            Log.v(TAG, "我是" + list.size() + bean.toString());
        }
        cursor.close();
        db.close();
        return list;
    }

    //删除操作
    public boolean deleteByPhone(String phone) {
        SQLiteDatabase db = helper.getWritableDatabase();
        int isDelete;
        String[] args;
        args = new String[]{phone};
        isDelete = db.delete(table_name, "phone=?", args);
        return isDelete > 0;
    }
}
