package com.richbond.sdjdsm.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.richbond.sdjdsm.bean.OrderBean;
import com.richbond.sdjdsm.util.DBHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * 物流单信息的dao方法
 * Created by LiWenxun on 2016/1/4.
 */
public class OrderDao {
    private static final String TAG = OrderDao.class.getSimpleName();
    private String table_name = DBHelper.TABLE_EMS;
    public static final String STATUS_NO = "no";
    public static final String STATUS_YES = "yes";
    DBHelper helper = null;

    public OrderDao(Context cxt) {
        helper = new DBHelper(cxt);
    }

    /**
     * 当Activity中调用此构造方法，传入一个版本号时，系统会在下一次调用数据库时调用Helper中的onUpgrade()方法进行更新
     *
     * @param cxt
     * @param version
     */
    public OrderDao(Context cxt, int version) {
        helper = new DBHelper(cxt, version);
    }

    /**
     * 将数据插入到表格中
     * @param bean
     * @return
     */
    public boolean insertData(OrderBean bean) {
        long isSucceed;
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("web", bean.getWeb());
        values.put("status", bean.getStatus());
        values.put("userid", bean.getUserid());
        values.put("phone", bean.getPhone());
        values.put("barcode", bean.getBarcode());
        values.put("customphone", bean.getCustomphone());
        values.put("customname", bean.getCustomname());
        values.put("customcard", bean.getCustomcard());
        values.put("latitude", bean.getLat());
        values.put("longitude", bean.getLng());
        values.put("hwlx", bean.getHwlx());
        values.put("hwlxbm", bean.getHwlxbm());
        values.put("rphone", bean.getRphone());
        values.put("photo_a", bean.getPhoto_a());
        values.put("appversion", bean.getAppversion());
        values.put("province", bean.getProvince());
        values.put("city", bean.getCity());
        values.put("county", bean.getCounty());
        values.put("address", bean.getAddress());
        values.put("iskxys", bean.getIskxys());
        values.put("iswxp", bean.getIswxp());
        values.put("iswjp", bean.getIswjp());
        values.put("gzlx", bean.getGzlx());
        values.put("systemname", bean.getSystemname());
        values.put("photo_b", bean.getPhoto_b());
        values.put("photo_c", bean.getPhoto_c());

        isSucceed = db.insert(table_name, null, values);
        db.close();
        return isSucceed > 0;
    }

    /**
     * 根据状态查询
     *
     * @param statusStr
     * @return
     */
    public List<OrderBean> findData(String statusStr) {
        List<OrderBean> list = new ArrayList<OrderBean>();
        String sql = "select * from " + table_name + " where status = '" + statusStr+"'";
        Log.v(TAG,"sql语句-----" + sql);
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            OrderBean bean = new OrderBean();
            //String name = cursor.getString(0); //获取第一列的值,第一列的索引从0开始
            String status = cursor.getString(cursor.getColumnIndex("status")); //
            String web = cursor.getString(cursor.getColumnIndex("web")); //
            String userid = cursor.getString(cursor.getColumnIndex("userid")); //
            String phone = cursor.getString(cursor.getColumnIndex("phone"));//
            String barcode = cursor.getString(cursor.getColumnIndex("barcode"));//
            String customphone = cursor.getString(cursor.getColumnIndex("customphone"));//
            String customname = cursor.getString(cursor.getColumnIndex("customname"));//
            String customcard = cursor.getString(cursor.getColumnIndex("customcard"));//
            String latitude = cursor.getString(cursor.getColumnIndex("latitude"));//
            String longitude = cursor.getString(cursor.getColumnIndex("longitude"));//
            String hwlx = cursor.getString(cursor.getColumnIndex("hwlx"));//
            String hwlxbm = cursor.getString(cursor.getColumnIndex("hwlxbm"));//
            String rphone = cursor.getString(cursor.getColumnIndex("rphone"));//
            String photo_a = cursor.getString(cursor.getColumnIndex("photo_a"));//
            String appversion = cursor.getString(cursor.getColumnIndex("appversion"));//
            String systemversion = cursor.getString(cursor.getColumnIndex("systemversion"));//
            String province = cursor.getString(cursor.getColumnIndex("province"));//
            String city = cursor.getString(cursor.getColumnIndex("city"));//
            String county = cursor.getString(cursor.getColumnIndex("county"));//
            String address = cursor.getString(cursor.getColumnIndex("address"));//
            String iskxys = cursor.getString(cursor.getColumnIndex("iskxys"));//
            String iswxp = cursor.getString(cursor.getColumnIndex("iswxp"));//
            String iswjp = cursor.getString(cursor.getColumnIndex("iswjp"));//
            String gzlx = cursor.getString(cursor.getColumnIndex("gzlx"));//
            String systemname = cursor.getString(cursor.getColumnIndex("systemname"));//
            String photo_b = cursor.getString(cursor.getColumnIndex("photo_b"));//
            String photo_c = cursor.getString(cursor.getColumnIndex("photo_c"));//

            bean.setWeb(web);
            bean.setStatus(status);
            bean.setUserid(userid);
            bean.setPhone(phone);
            bean.setBarcode(barcode);
            bean.setCustomphone(customphone);
            bean.setCustomname(customname);
            bean.setCustomcard(customcard);
            bean.setLat(latitude);
            bean.setLng(longitude);
            bean.setHwlx(hwlx);
            bean.setHwlxbm(hwlxbm);
            bean.setRphone(rphone);
            bean.setPhoto_a(photo_a);
            bean.setAppversion(appversion);
            bean.setSystemversion(systemversion);
            bean.setProvince(province);
            bean.setCity(city);
            bean.setCounty(county);
            bean.setAddress(address);
            bean.setIskxys(iskxys);
            bean.setIswxp(iswxp);
            bean.setIswjp(iswjp);
            bean.setGzlx(gzlx);
            bean.setSystemname(systemname);
            bean.setPhoto_b(photo_b);
            bean.setPhoto_c(photo_c);

            list.add(bean);
        }
        cursor.close();
        db.close();
        return list;
    }

    // 查询操作
    public List<OrderBean> findDataByBarcode(String barcodeStr) {
        List<OrderBean> list = new ArrayList<OrderBean>();
        String sql = "select * from " + table_name + " where (barcode like '%" + barcodeStr + "%')";
        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            OrderBean bean = new OrderBean();
            //String name = cursor.getString(0); //获取第一列的值,第一列的索引从0开始
            String status = cursor.getString(cursor.getColumnIndex("status")); //
            String web = cursor.getString(cursor.getColumnIndex("web")); //
            String userid = cursor.getString(cursor.getColumnIndex("userid")); //
            String phone = cursor.getString(cursor.getColumnIndex("phone"));//
            String barcode = cursor.getString(cursor.getColumnIndex("barcode"));//
            String customphone = cursor.getString(cursor.getColumnIndex("customphone"));//
            String customname = cursor.getString(cursor.getColumnIndex("customname"));//
            String customcard = cursor.getString(cursor.getColumnIndex("customcard"));//
            String latitude = cursor.getString(cursor.getColumnIndex("latitude"));//
            String longitude = cursor.getString(cursor.getColumnIndex("longitude"));//
            String hwlx = cursor.getString(cursor.getColumnIndex("hwlx"));//
            String hwlxbm = cursor.getString(cursor.getColumnIndex("hwlxbm"));//
            String rphone = cursor.getString(cursor.getColumnIndex("rphone"));//
            String photo_a = cursor.getString(cursor.getColumnIndex("photo_a"));//
            String appversion = cursor.getString(cursor.getColumnIndex("appversion"));//
            String systemversion = cursor.getString(cursor.getColumnIndex("systemversion"));//

            String province = cursor.getString(cursor.getColumnIndex("province"));//
            String city = cursor.getString(cursor.getColumnIndex("city"));//
            String county = cursor.getString(cursor.getColumnIndex("county"));//
            String address = cursor.getString(cursor.getColumnIndex("address"));//
            String iskxys = cursor.getString(cursor.getColumnIndex("iskxys"));//
            String iswxp = cursor.getString(cursor.getColumnIndex("iswxp"));//
            String iswjp = cursor.getString(cursor.getColumnIndex("iswjp"));//
            String gzlx = cursor.getString(cursor.getColumnIndex("gzlx"));//
            String systemname = cursor.getString(cursor.getColumnIndex("systemname"));//
            String photo_b = cursor.getString(cursor.getColumnIndex("photo_b"));//
            String photo_c = cursor.getString(cursor.getColumnIndex("photo_c"));//

            bean.setWeb(web);
            bean.setStatus(status);
            bean.setUserid(userid);
            bean.setPhone(phone);
            bean.setBarcode(barcode);
            bean.setCustomphone(customphone);
            bean.setCustomname(customname);
            bean.setCustomcard(customcard);
            bean.setLat(latitude);
            bean.setLng(longitude);
            bean.setHwlx(hwlx);
            bean.setHwlxbm(hwlxbm);
            bean.setRphone(rphone);
            bean.setPhoto_a(photo_a);
            bean.setAppversion(appversion);
            bean.setSystemversion(systemversion);

            bean.setProvince(province);
            bean.setCity(city);
            bean.setCounty(county);
            bean.setAddress(address);
            bean.setIskxys(iskxys);
            bean.setIswxp(iswxp);
            bean.setIswjp(iswjp);
            bean.setGzlx(gzlx);
            bean.setSystemname(systemname);
            bean.setPhoto_b(photo_b);
            bean.setPhoto_c(photo_c);

            list.add(bean);
        }
        cursor.close();
        db.close();
        return list;
    }

    /**
     * 删除操作(根据物流单号)
     *
     * @param barcode
     * @return
     */
    public boolean deleteByBarcode(String barcode) {
        SQLiteDatabase db = helper.getWritableDatabase();
        int isDelete;
        String[] args = new String[]{barcode};
        isDelete = db.delete(table_name, "barcode=?", args);
        return isDelete > 0;
    }

    /**
     * 修改操作 修改快递单的状态
     *
     * @param barcode
     * @param statusStr
     * @return
     */
    public boolean updateStatus(String barcode, String statusStr) {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("status", statusStr);//key为字段名，value为值
        String[] args = new String[]{barcode};
        int isSucceed = db.update(table_name, values, "barcode=? ", args);
        db.close();
        return isSucceed > 0;
    }
}
