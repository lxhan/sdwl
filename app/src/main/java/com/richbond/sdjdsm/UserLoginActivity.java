/**
 * @Title: UserLoginActivity.java
 * @Package com.uroad.cscxy
 * @Description: TODO(用一句话描述该文件做什么)
 * @author oupy
 * @date 2014-5-24 下午2:49:35
 * @version V1.0
 */
package com.richbond.sdjdsm;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.richbond.sdjdsm.common.BaseActivity;
import com.richbond.sdjdsm.sharedpreferences.UserSharedPreferences;
import com.richbond.sdjdsm.util.DialogHelper;
import com.richbond.sdjdsm.util.JsonUtil;
import com.richbond.sdjdsm.util.UIHelp;
import com.richbond.sdjdsm.webserver.PoiWS;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Administrator
 *         用户登录界面
 */
public class UserLoginActivity extends BaseActivity {

    @Bind(R.id.etUserName)
    EditText etUserName;
    @Bind(R.id.etPwd)
    EditText etPwd;
    private boolean isTask = false;
    private PoiWS ws;


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setBaseContentLayout(R.layout.activity_userlogin);
        ButterKnife.bind(this);
        init();

    }

    private void init() {
        setTitle("登录");
        ws = new PoiWS(this);
        etUserName.setText("13823274469");
        etPwd.setText("123456");
    }

    @OnClick({R.id.btnLogin})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (UIHelp.isNetworkConnected(this)){
                    login();
                }
                break;
        }
    }

    private void login() {
        if (isTask) {
            return;
        }
        if (etUserName.getText().toString().equalsIgnoreCase("") || etUserName.getText().toString().length() != 11) {
            etUserName.setError("请填写正确手机号码");
            etUserName.requestFocus();
            return;
        }
        if (etPwd.getText().toString().length() < 6) {
            etPwd.setError("请输入6位以上密码");
            etUserName.requestFocus();
            return;
        }
        String phone = etUserName.getText().toString();
        String password = etPwd.getText().toString();

        new LoginTask().execute(phone, password);
    }

    class LoginTask extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            return ws.userLogin(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            Log.i("userLogin=========", String.valueOf(result));
            isTask = false;
            DialogHelper.closeProgressDialog();
            if (JsonUtil.GetJsonStatu(result)) {
                try {
                    JSONObject res = result.getJSONObject("data");
                    UserSharedPreferences.login(UserLoginActivity.this, res);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(UserLoginActivity.this, MainActivity.class);
                startActivity(intent);
                DialogHelper.showToast(UserLoginActivity.this, "登陆成功");
            } else {
                DialogHelper.showToast(UserLoginActivity.this, JsonUtil.GetString(result, "msg"));
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

    }
}
