package com.richbond.sdjdsm.bean;

/**
 * 货物名称实体类
 * Created by Administrator on 2016/4/14.
 */
public class HwlxBean {
    private String mc;//货物类型名称
    private String id;//货物类型id
    private String dm;//货物类型代码
    private String preant_dm;//
    private String isvalid;//是否可用

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDm() {
        return dm;
    }

    public void setDm(String dm) {
        this.dm = dm;
    }

    public String getPreant_dm() {
        return preant_dm;
    }

    public void setPreant_dm(String preant_dm) {
        this.preant_dm = preant_dm;
    }

    public String getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(String isvalid) {
        this.isvalid = isvalid;
    }

    @Override
    public String toString() {
        return mc;
    }
}
