package com.richbond.sdjdsm.bean;

/**
 * 用户信息实体类（快递人员）
 * Created by Administrator on 2016/4/14.
 */
public class UserBean extends BaseBean {
    private String isLogin = "0";
    private String id;
    private String username;
    private String password;
    private String phone;

    public String getIsLogin() {
        return isLogin;
    }

    public void setIsLogin(String isLogin) {
        this.isLogin = isLogin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
