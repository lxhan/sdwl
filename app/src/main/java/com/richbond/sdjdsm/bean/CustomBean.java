package com.richbond.sdjdsm.bean;

/**
 * 寄件人实体类（寄件人手机号码，寄件人姓名，寄件人身份证号码）
 * Created by LuHan on 2016/1/5.
 */
public class CustomBean {
    public int id;
    public String name;//快递人员姓名
    public String phone;//联系电话
    public String idcard;//身份证

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }
}
