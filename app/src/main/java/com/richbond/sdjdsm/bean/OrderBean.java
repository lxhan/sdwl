package com.richbond.sdjdsm.bean;

import com.richbond.sdjdsm.MainActivity;
import com.richbond.sdjdsm.dao.OrderDao;

/**
 * 要提交的信息实体
 * Created by Administrator on 2016/4/13.
 */
public class OrderBean extends BaseBean {
    private String status = OrderDao.STATUS_NO;//快递状态
    private String userid = MainActivity.s_userID;//用户id
    private String phone = MainActivity.s_userPhone;//用户手机
    private String barcode;//条码
    private String customphone;//寄件人手机号码
    private String customname;//寄件人姓名
    private String customcard;//寄件人身份证号码
    private String lat;//纬度
    private String lng;//经度
    private String hwlx;//货物类型
    private String hwlxbm;//货物类型代码
    private String rphone;//收件人手机号码
    private String web;//
    private String appversion = "1";//app版本
    private String systemversion = android.os.Build.VERSION.RELEASE;//用户手机版本号
    private String arriveprovince;//到站省市区 （例如：湖南省长沙市芙蓉区）
    private String province;//到站省份
    private String city;//到站城市
    private String county;//到站县、区
    private String address;//详细到站地址（例如：蔡锷路222路同福公寓2315房）
    private String iskxys;//是否开箱验视 （是/否）
    private String iswxp;//是否危险品 （是/否）
    private String iswjp;//是否违禁品 （是/否）
    private String gzlx;//贵重类型 （贵重/一般）
    private String systemname = "android";//系统名称
    private String photo_a;//照片
    private String photo_b;//照片
    private String photo_c;//照片

    public OrderBean() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCustomphone() {
        return customphone;
    }

    public void setCustomphone(String customphone) {
        this.customphone = customphone;
    }

    public String getCustomname() {
        return customname;
    }

    public void setCustomname(String customname) {
        this.customname = customname;
    }

    public String getCustomcard() {
        return customcard;
    }

    public void setCustomcard(String customcard) {
        this.customcard = customcard;
    }

    public String getHwlxbm() {
        return hwlxbm;
    }

    public void setHwlxbm(String hwlxbm) {
        this.hwlxbm = hwlxbm;
    }

    public String getRphone() {
        return rphone;
    }

    public void setRphone(String rphone) {
        this.rphone = rphone;
    }

    public String getPhoto_a() {
        return photo_a;
    }

    public void setPhoto_a(String photo_a) {
        this.photo_a = photo_a;
    }

    public String getPhoto_b() {
        return photo_b;
    }

    public void setPhoto_b(String photo_b) {
        this.photo_b = photo_b;
    }

    public String getPhoto_c() {
        return photo_c;
    }

    public void setPhoto_c(String photo_c) {
        this.photo_c = photo_c;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getAppversion() {
        return appversion;
    }

    public void setAppversion(String appversion) {
        this.appversion = appversion;
    }

    public String getSystemversion() {
        return systemversion;
    }

    public void setSystemversion(String systemversion) {
        this.systemversion = systemversion;
    }

    public String getArriveprovince() {
        return arriveprovince;
    }

    public void setArriveprovince(String arriveprovince) {
        this.arriveprovince = arriveprovince;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIskxys() {
        return iskxys;
    }

    public void setIskxys(String iskxys) {
        this.iskxys = iskxys;
    }

    public String getIswxp() {
        return iswxp;
    }

    public void setIswxp(String iswxp) {
        this.iswxp = iswxp;
    }

    public String getIswjp() {
        return iswjp;
    }

    public void setIswjp(String iswjp) {
        this.iswjp = iswjp;
    }

    public String getGzlx() {
        return gzlx;
    }

    public void setGzlx(String gzlx) {
        this.gzlx = gzlx;
    }

    public String getSystemname() {
        return systemname;
    }

    public void setSystemname(String systemname) {
        this.systemname = systemname;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getHwlx() {
        return hwlx;
    }

    public void setHwlx(String hwlx) {
        this.hwlx = hwlx;
    }

    @Override
    public String toString() {
        return "OrderBean{" +
                "status='" + status + '\'' +
                ", userid='" + userid + '\'' +
                ", phone='" + phone + '\'' +
                ", barcode='" + barcode + '\'' +
                ", customphone='" + customphone + '\'' +
                ", customname='" + customname + '\'' +
                ", customcard='" + customcard + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", hwlx='" + hwlx + '\'' +
                ", hwlxbm='" + hwlxbm + '\'' +
                ", rphone='" + rphone + '\'' +
                ", web='" + web + '\'' +
                ", appversion='" + appversion + '\'' +
                ", systemversion='" + systemversion + '\'' +
                ", arriveprovince='" + arriveprovince + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", address='" + address + '\'' +
                ", iskxys='" + iskxys + '\'' +
                ", iswxp='" + iswxp + '\'' +
                ", iswjp='" + iswjp + '\'' +
                ", gzlx='" + gzlx + '\'' +
                ", systemname='" + systemname + '\'' +
                ", photo_a='" + photo_a + '\'' +
                ", photo_b='" + photo_b + '\'' +
                ", photo_c='" + photo_c + '\'' +
                '}';
    }
}
