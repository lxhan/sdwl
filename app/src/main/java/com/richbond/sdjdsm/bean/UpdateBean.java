package com.richbond.sdjdsm.bean;

/**
 * 更新信息实体类
 * Created by Administrator on 2016/4/25.
 */
public class UpdateBean {
    /**
     * app版本号（用于判断是否需要升级）
     */
    private String appVersionCode;

    /**
     * app版本名称
     */
    private String appVersionName;

    /**
     * app下载地址
     */
    private String appDownloadUrl;

    /**
     * 本app用到的接口地址
     */
    private String webInterface;

    public String getAppVersionCode() {
        return appVersionCode;
    }

    public void setAppVersionCode(String appVersionCode) {
        this.appVersionCode = appVersionCode;
    }

    public String getAppVersionName() {
        return appVersionName;
    }

    public void setAppVersionName(String appVersionName) {
        this.appVersionName = appVersionName;
    }

    public String getAppDownloadUrl() {
        return appDownloadUrl;
    }

    public void setAppDownloadUrl(String appDownloadUrl) {
        this.appDownloadUrl = appDownloadUrl;
    }

    public String getWebInterface() {
        return webInterface;
    }

    public void setWebInterface(String webInterface) {
        this.webInterface = webInterface;
    }

    @Override
    public String toString() {
        return "UpdateBean{" +
                "appVersionCode='" + appVersionCode + '\'' +
                ", appVersionName='" + appVersionName + '\'' +
                ", appDownloadUrl='" + appDownloadUrl + '\'' +
                ", webInterface='" + webInterface + '\'' +
                '}';
    }
}

