package com.richbond.sdjdsm.entity;

import android.os.Bundle;

/**
 * Created by liwenxun on 15/8/12.
 * OptimalFragmentAdapter　系列使用到的　数据模型类
 */
public class FragmentInfo {
    private Class<?> clss;
    private Bundle args;

    public FragmentInfo( Class<?> _class, Bundle _args) {
        clss = _class;
        args = _args;
    }

    /**
     * @return the args
     */
    public Bundle getArgs() {
        return args;
    }

    /**
     * @param args
     *            the args to set
     */
    public void setArgs(Bundle args) {
        this.args = args;
    }

    /**
     * @return the clss
     */
    public  Class<?> getClss() {
        return clss;
    }

    /**
     * @param clss
     *            the clss to set
     */
    public void setClss( Class<?> clss) {
        this.clss = clss;
    }

}