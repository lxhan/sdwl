package com.richbond.sdjdsm.entity;

/**
 * 省市区的实体类
 * Created by Administrator on 2016/4/18.
 */
public class RegionBean {
    private String region_id;
    private String name;
    private String level;
    private String p_region_id;

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getP_region_id() {
        return p_region_id;
    }

    public void setP_region_id(String p_region_id) {
        this.p_region_id = p_region_id;
    }
}
