package com.richbond.sdjdsm.widget.image;

import java.io.OutputStream;

/*
 * 下载图片的接口
 * */
public interface Downloader  {
	
	/**
	 * 请求网络的inputStream填充outputStream
	 * @param urlString
	 * @param outputStream
	 * @return
	 */
	public boolean downloadToLocalStreamByUrl(String urlString, OutputStream outputStream);
}
