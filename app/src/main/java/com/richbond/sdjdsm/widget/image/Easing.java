package com.richbond.sdjdsm.widget.image;

/*
 * 图片显示的动画效果接口
 * */
public interface Easing {

	double easeOut(double time, double start, double end, double duration);

	double easeIn(double time, double start, double end, double duration);

	double easeInOut(double time, double start, double end, double duration);
}
