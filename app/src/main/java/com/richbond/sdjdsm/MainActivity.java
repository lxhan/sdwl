package com.richbond.sdjdsm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewConfiguration;

import com.richbond.sdjdsm.adapter.OptimalTabPageIndicatorFragmentAdapter;
import com.richbond.sdjdsm.common.BaseFragmentActivity;
import com.richbond.sdjdsm.entity.FragmentInfo;
import com.richbond.sdjdsm.fragments.HomeFragment;
import com.richbond.sdjdsm.fragments.MyUserFragment;
import com.richbond.sdjdsm.fragments.SettingFragment;
import com.richbond.sdjdsm.util.CropHelper;
import com.richbond.sdjdsm.widget.ChangeColorIconWithTextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseFragmentActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {


    public static String s_userPhone, s_userID;
    private ViewPager mViewPager;
    private List<ChangeColorIconWithTextView> mTabIndicator = new ArrayList<ChangeColorIconWithTextView>();
    private OptimalTabPageIndicatorFragmentAdapter adapter;
    List<FragmentInfo> dataFragmentInfos;
    Class<?>[] fragmentClasses = {HomeFragment.class, MyUserFragment.class, SettingFragment.class};
    String[] titles = {"首页", "我的", "设置"};

//    public  SubmittedDao submittedDao =  new SubmittedDao(this);
//    public PersonDao personDao =  new PersonDao(this);
//    public NotSubmittedDao notSubmittedDao =  new NotSubmittedDao(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBaseContentLayout(R.layout.activity_main);


        setOverflowShowingAlways();

        init();
        mViewPager = (ViewPager) findViewById(R.id.id_viewpager);
        mViewPager.setOffscreenPageLimit(3);

        initTabIndicator();

        mViewPager.setAdapter(adapter);
        mViewPager.setOnPageChangeListener(this);
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v(HomeFragment.TAG, "MainActivity---requestCode" + requestCode + " resultCode" + resultCode);
        if (requestCode == CropHelper.REQUEST_CROP) {
            HomeFragment.pic_flag = true;
        }
    }*/

    private void init() {
        setTitle("寄递实名登记");
        HideLeftBtn();
        dataFragmentInfos = new ArrayList<FragmentInfo>();
        for (int i = 0; i < fragmentClasses.length; i++) {
            Bundle bundle = new Bundle();
            bundle.putString("dd", "dd");
            FragmentInfo info = new FragmentInfo(fragmentClasses[i], bundle);
            dataFragmentInfos.add(info);
        }
        adapter = new OptimalTabPageIndicatorFragmentAdapter(this, getSupportFragmentManager(), dataFragmentInfos, titles);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void initTabIndicator() {
        ChangeColorIconWithTextView one = (ChangeColorIconWithTextView) findViewById(R.id.id_indicator_one);
        ChangeColorIconWithTextView two = (ChangeColorIconWithTextView) findViewById(R.id.id_indicator_two);
        ChangeColorIconWithTextView three = (ChangeColorIconWithTextView) findViewById(R.id.id_indicator_three);

        mTabIndicator.add(one);
        mTabIndicator.add(two);
        mTabIndicator.add(three);

        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);

        one.setIconAlpha(1.0f);
    }

    @Override
    public void onPageSelected(int arg0) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
                               int positionOffsetPixels) {
        // Log.e("TAG", "position = " + position + " , positionOffset = "
        // + positionOffset);

        if (positionOffset > 0) {
            ChangeColorIconWithTextView left = mTabIndicator.get(position);
            ChangeColorIconWithTextView right = mTabIndicator.get(position + 1);

            left.setIconAlpha(1 - positionOffset);
            right.setIconAlpha(positionOffset);
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {

        resetOtherTabs();

        switch (v.getId()) {
            case R.id.id_indicator_one:
                mTabIndicator.get(0).setIconAlpha(1.0f);
                mViewPager.setCurrentItem(0, false);
                break;
            case R.id.id_indicator_two:
                mTabIndicator.get(1).setIconAlpha(1.0f);
                mViewPager.setCurrentItem(1, false);
                break;
            case R.id.id_indicator_three:
                mTabIndicator.get(2).setIconAlpha(1.0f);
                mViewPager.setCurrentItem(2, false);
                break;

        }

    }

    /**
     * 重置其他的Tab
     */
    private void resetOtherTabs() {
        for (int i = 0; i < mTabIndicator.size(); i++) {
            mTabIndicator.get(i).setIconAlpha(0);
        }
    }


    private void setOverflowShowingAlways() {
        try {
            // true if a permanent menu key is present, false otherwise.
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            menuKeyField.setAccessible(true);
            menuKeyField.setBoolean(config, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
