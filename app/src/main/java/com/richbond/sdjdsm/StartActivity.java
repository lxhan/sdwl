package com.richbond.sdjdsm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;

import com.richbond.sdjdsm.sharedpreferences.HwlxSharedPreferences;
import com.richbond.sdjdsm.util.SystemUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by liwenxun on 15/9/6.
 */
public class StartActivity extends Activity {
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_start));
        checkoutHwlx();
        init();
    }

    /**
     * 如果货物类型列表从未初始化过，则添加自定义的货物类型
     */
    private void checkoutHwlx() {
        if (HwlxSharedPreferences.find(this).size()==0){
            String hwlxStr = SystemUtil.getFromAssets(this, "hwlxOriginal.txt");
            try {
                JSONObject jsonObject = new JSONObject(hwlxStr);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                HwlxSharedPreferences.add(jsonArray.toString(), this);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void init() {

        //1秒后　进入主页
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                redirectTo();
            }
        }, 1000);
    }
    /**
     * 跳转到...
     */
    private void redirectTo(){
        startActivity(new Intent(StartActivity.this, MainActivity.class));
        finish();
    }
    @Override
    public void finish() {
        super.finish();
    }
    /**
     * @todo 重写onKeyDown方法
     * @author LuHan
     * create by 2015/11/1 21:27
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {//防止开屏界面点返回键退出
            return true;
        }
        return false;
    }

}
