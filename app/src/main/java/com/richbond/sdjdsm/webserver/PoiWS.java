/**
 * @Title: PoiWS.java
 * @Package com.uroad.cscxy.webserver
 * @Description: TODO(用一句话描述该文件做什么)
 * @author oupy
 * @date 2014-5-25 上午8:28:30
 * @version V1.0
 */
package com.richbond.sdjdsm.webserver;

import android.content.Context;
import android.util.Log;

import com.richbond.decrypt.Des;
import com.richbond.decrypt.EncryptUtil;
import com.richbond.sdjdsm.bean.OrderBean;
import com.richbond.sdjdsm.net.RequestParams;
import com.richbond.sdjdsm.net.SyncHttpClient;
import com.richbond.sdjdsm.util.BeanToParams;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.List;

/**
 * @author Administrator
 *         poi类接口
 */
public class PoiWS extends CXYBaseWS {
    private static final String TAG = PoiWS.class.getSimpleName();
    String address_app = "http://220.170.145.245:18082/JMT/app/";
    String url_sdwl = "http://120.27.193.242:85/";

    /**
     * @param context
     */
    public PoiWS(Context context) {
        super(context);
    }

    /*
* 用户登录
*
* */
    public JSONObject userLogin(String phone, String password) {
        //String url = web + "/login.do";
        String url = url_sdwl + "sdwl/login.do";
        SyncHttpClient client = new SyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("phone", phone);
        params.put("password", password);

        return client.postToJson(url, encryptRequestParams(params));
    }


    /*
* 获取货物类型列表
*
* */
    public JSONObject getContents(String phone) {
        //String url = web + "/content/getContents.do";
        String url = url_sdwl + "sdwl/content/getContents.do";
        SyncHttpClient client = new SyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("phone", phone);
        return client.postToJson(url, encryptRequestParams(params));
    }

    /*
* 提交快件
*
* */
    public JSONObject sendInfo(OrderBean bean) {
        //String url = bean.getWeb() + "/parcel/addParcel.do";
        Log.v(TAG, "sendInfo里面" + bean.toString());
        String url = url_sdwl + "sdwl/parcel/addParcel.do";
        SyncHttpClient client = new SyncHttpClient();
        RequestParams params = BeanToParams.getRequestParams(bean);
        return client.postToJson(url, encryptRequestParams(params));
    }


    /*
* 更新
*
* */
    public JSONObject Update() {
        //String url = "http://120.27.193.242:82/app/sd_sdwl/sdwl.xml?temp="+System.currentTimeMillis();
        //String url = "http://121.43.185.200:82/app/yz_kdsm/kdsm.xml";
        String url = "http://120.27.193.242:82/version.do?target=sd_wljdy&systemType=android";
        SyncHttpClient client = new SyncHttpClient();
        return client.postToJson(url);
    }

    /*
* 获取xml
*
* */
    public String getXml() {
        Log.v(TAG, "getXml");
        String url = "http://kdsmpt.com/config/config.xml?temp="+System.currentTimeMillis();
        SyncHttpClient client = new SyncHttpClient();
        return client.post(url);
    }

    /*
* 保存用户头像
*
* */
    public JSONObject userSavePicture(String userId, String imagedata) {
        String url = "http://220.170.145.245:18082/JMT/manage/User_userSavePicture.do";
        SyncHttpClient client = new SyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("imagedata", imagedata);

        return client.postToJson(url, params);
    }

    /*
* 获取用户头像
*
* */
    public JSONObject getPictureInfo(String userId) {
        String url = "http://220.170.145.245:18082/JMT/manage/User_getPictureInfo.do";
        SyncHttpClient client = new SyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("userId", userId);

        return client.postToJson(url, params);
    }

    /*
* 修改密码获取验证码
*
* */
    public JSONObject identifyCodeWhenPassword(String userPhone) {
        String url = address_app + "App_identifyCodeWhenPassword.do";
        SyncHttpClient client = new SyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("userPhone", userPhone);

        return client.postToJson(url, params);
    }

    /*
* 修改密码验证获取的验证码
*
* */
    public JSONObject verifyCodeWhenPassword(String userPhone, String verifyCode) {
        String url = address_app + "App_verifyCodeWhenPassword.do";
        SyncHttpClient client = new SyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("userPhone", userPhone);
        params.put("verifyCode", verifyCode);

        return client.postToJson(url, params);
    }

    /*
* 修改密码
*
* */
    public JSONObject changePassword(String userPhone, String userPassword) {
        String url = address_app + "App_changePassword.do";
        SyncHttpClient client = new SyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("userPhone", userPhone);
        params.put("userPassword", userPassword);

        return client.postToJson(url, params);

    }

    /*
* 意见反馈
*
* */
    public JSONObject sendSuggestion(String phone, String content) {
        String url = address_app + "App_sendSuggestion.do";
        SyncHttpClient client = new SyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("phone", phone);
        params.put("content", content);

        return client.postToJson(url, params);
    }

    public static RequestParams encryptRequestParams(RequestParams requestParams) {
        List<BasicNameValuePair> list = requestParams.getParamsList();
        String secretKey = EncryptUtil.getSecretKey();
        Des des = new Des(secretKey);
        RequestParams rp = new RequestParams();
        for (BasicNameValuePair bp : list) {
            String key = bp.getName();
            if (ignoreKey(bp.getName())) {
                rp.put(key, bp.getValue());
                continue;
            } else {
                try {
                    rp.put(key, des.encrypt(bp.getValue()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        rp.put("sign", EncryptUtil.encryptByRsa(secretKey));
        return rp;
    }

    private static boolean ignoreKey(String targetKey) {
        boolean a = "action".equalsIgnoreCase(targetKey);
        boolean b = "resultType".equalsIgnoreCase(targetKey);
        boolean c = "ywxtmc".equalsIgnoreCase(targetKey);
        return a || b || c;
    }
}


