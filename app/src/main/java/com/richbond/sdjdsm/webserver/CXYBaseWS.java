/**  
* @Title: CXYBaseWS.java
* @Package com.uroad.cxy.webservice
* @Description: TODO(用一句话描述该文件做什么)
* @author oupy 
* @date 2013-11-28 上午11:31:36
* @version V1.0  
*/
package com.richbond.sdjdsm.webserver;

import android.content.Context;

import com.richbond.sdjdsm.net.PersistentCookieStore;
import com.richbond.sdjdsm.net.RequestParams;
import com.richbond.sdjdsm.net.SyncHttpClient;
import com.richbond.sdjdsm.util.SecurityUtil;


/**
 * @author Administrator
 * 调用接口的所以接口类的父类，定位的接口的地址和加密解密方法
 */
public class CXYBaseWS extends BaseWebService {
	

	Context mContext;

	public CXYBaseWS(Context context) {
		mContext = context;
	}
	
	 
	public static String serverIp = "113.240.245.70:8080";
	public static String PIC_URL = "http://" + serverIp + "/gde/picture/";
	public static String Method_URL = "http://" + serverIp
			+ "/CSCXYAPIServer/index.php?/";
	
	
	protected String GetMethodURL(String str) {
		return GetMethodURL(serverIp, str, "http");
	}

	protected String GetMethodURL(String ip, String str, String httpType) {
		return httpType + "://" + ip + str;
	}

	protected String GetMethodURL(String str, String httpType) {
		return GetMethodURL(serverIp, str, httpType);
	}

	public static String GetStaticMethodURLByFunCode(String funCode) {
		return Method_URL + funCode;
	}

	public String GetMethodURLByFunCode(String funCode) {
		return Method_URL + funCode;
	}
	
	
	/*
	 * 键权方式，调用接口的基本参数
	 * 
	 * 
	 * */
	public RequestParams getParams() {
		RequestParams params = new RequestParams();
		params.put("timestamp", System.currentTimeMillis() / 1000 + "");
		params.put("publickey", "2e6ac265e22b2cfbf8f02d4803e1edb57c9b3b55");
		params.put(
				"accesstoken",
				SecurityUtil.toHMACSHA256String(System.currentTimeMillis()
						/ 1000 + "2e6ac265e22b2cfbf8f02d4803e1edb57c9b3b55", "de26f39add253c19097308fb0aa4a7ffad8c919e"));

		return params;
	}
	
	
	/*
	 * 启用了cookie的同步synchttpclient
	 * */
	public SyncHttpClient getAsyncHttpClient() {
		SyncHttpClient httpClient = new SyncHttpClient();
		PersistentCookieStore myCookieStore = new PersistentCookieStore(
				mContext);
		httpClient.setCookieStore(myCookieStore);
		return httpClient;
	}

	
}
