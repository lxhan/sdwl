package com.richbond.sdjdsm;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.richbond.sdjdsm.bean.OrderBean;
import com.richbond.sdjdsm.common.BaseActivity;
import com.richbond.sdjdsm.dao.OrderDao;
import com.richbond.sdjdsm.util.DialogHelper;
import com.richbond.sdjdsm.util.StringUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SubmittedInfoActivity extends BaseActivity {

    @Bind(R.id.tvTxm)
    TextView tvTxm;//物流单号
    @Bind(R.id.tvSjhm)
    TextView tvSjhm;//寄件人手机号码
    @Bind(R.id.tvXm)
    TextView tvXm;//寄件人姓名
    @Bind(R.id.tvSfz)
    TextView tvSfz;//寄件人身份证号码
    @Bind(R.id.tvNj)
    TextView tvNj;//货物类型名称
    @Bind(R.id.tvSjrhm)
    TextView tvSjrhm;//收件人手机号码

    private String barcode = "";
    public OrderDao orderDao;
    public OrderBean orderBean;


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setBaseContentLayout(R.layout.activity_submitinfo);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        setTitle("物流单详情");
        setRightBtn("删除", R.color.transparent, 80, 50);
        orderDao = new OrderDao(this);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            barcode = bundle.getString("barcode");
            orderBean = orderDao.findDataByBarcode(barcode).get(0);
        } else {
            orderBean = new OrderBean();
        }
        tvSfz.setText(StringUtils.getHideCharacter(orderBean.getCustomcard()));//寄件人身份证号码
        tvSjhm.setText(StringUtils.getHideCharacter(orderBean.getCustomphone()));//寄件人手机号码
        tvXm.setText(StringUtils.getHideCharacter(orderBean.getCustomname()));//寄件人姓名
        tvSjrhm.setText(StringUtils.getHideCharacter(orderBean.getRphone()));//收件人手机号码
        tvTxm.setText(barcode);//物流单号
        tvNj.setText(orderBean.getHwlx());//货物类型
    }

    @Override
    protected void onRightClick(View v) {
        if (orderDao.deleteByBarcode(barcode)) {
            DialogHelper.showToast(this, "删除成功");
            this.finish();
        }
    }
}
