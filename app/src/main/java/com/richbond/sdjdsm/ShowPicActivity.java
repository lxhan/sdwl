package com.richbond.sdjdsm;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.polites.android.GestureImageView;
import com.richbond.sdjdsm.fragments.HomeFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowPicActivity extends Activity {
    /**
     * 查看大图片的code
     */
    public static final int RESULT_CODE_PIC = 122;
    /**
     * 自定义de图片控件（可缩放）
     */
    @Bind(R.id.pic)
    GestureImageView gestureImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_show_pic);
        ButterKnife.bind(this);
        initPic();
    }

    /**
     * 将图片显示出来 读取路径
     */
    private void initPic() {
        Bitmap imageBitmap = BitmapFactory.decodeFile(HomeFragment.PHOTO_DIR + "/" + HomeFragment.PHOTO_NAME);
        gestureImageView.setImageBitmap(imageBitmap);
    }

    @OnClick({R.id.btn_pic_cancel, R.id.btn_pic_delete})
    public void OnClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.btn_pic_cancel://返回按钮
                intent.putExtra("flag", "cancel");
                setResult(RESULT_CODE_PIC, intent);
                this.finish();
                break;
            case R.id.btn_pic_delete://删除按钮
                intent.putExtra("flag", "delete");
                setResult(RESULT_CODE_PIC, intent);
                this.finish();
                break;
        }
    }
}
