package com.richbond.sdjdsm.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.richbond.sdjdsm.MainActivity;
import com.richbond.sdjdsm.NotSubmittedActivity;
import com.richbond.sdjdsm.R;
import com.richbond.sdjdsm.SubmittedActivity;
import com.richbond.sdjdsm.UserLoginActivity;
import com.richbond.sdjdsm.bean.OrderBean;
import com.richbond.sdjdsm.bean.UserBean;
import com.richbond.sdjdsm.common.BaseFragment;
import com.richbond.sdjdsm.dao.OrderDao;
import com.richbond.sdjdsm.sharedpreferences.UserSharedPreferences;
import com.richbond.sdjdsm.util.DialogHelper;
import com.richbond.sdjdsm.widget.RoundImageView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 主界面：我的
 * Created by liwenxun on 15/8/13.
 */
public class MyUserFragment extends BaseFragment {

    @Bind(R.id.btnLogin)
    Button btnLogin;
    @Bind(R.id.btnLogout)
    Button btnLogout;
    @Bind(R.id.llLogin)
    LinearLayout llLogin;
    @Bind(R.id.llLogout)
    LinearLayout llLogout;
    @Bind(R.id.llMyAccount)
    LinearLayout llMyAccount;
    @Bind(R.id.llMySt)
    LinearLayout llMySt;
    @Bind(R.id.rvHead)
    RoundImageView rvHead;
    @Bind(R.id.tvName)
    TextView tvName;
    @Bind(R.id.tvXiaoyuan)
    TextView tvXiaoyuan;

    private List<OrderBean> list = null;
    public OrderDao orderDao;
    private UserBean userBean = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = setBaseContentLayout(R.layout.fragment_myuser);
        ButterKnife.bind(this, view);
        userBean = UserSharedPreferences.find(getActivity());
        init(view);

    }

    @Override
    public void onResume() {
        super.onResume();
        userBean = UserSharedPreferences.find(getActivity());
        if (userBean.getIsLogin().equalsIgnoreCase("0")) {
            llLogin.setVisibility(View.VISIBLE);
            llLogout.setVisibility(View.GONE);
            rvHead.setImageResource(R.drawable.user_head);
            tvName.setText("未登录");
        } else {
            llLogout.setVisibility(View.VISIBLE);
            llLogin.setVisibility(View.GONE);
            tvName.setText(userBean.getUsername());
            MainActivity.s_userID = userBean.getId();
            MainActivity.s_userPhone = userBean.getPhone();
            list = null;
            list = orderDao.findData(OrderDao.STATUS_NO);
            Log.e("list ===", list.size()+"");

            if (list.size() > 0) {
                if (list.size() > 99) {
                    tvXiaoyuan.setText("99");
                } else {
                    tvXiaoyuan.setText(String.valueOf(list.size()));
                }
                tvXiaoyuan.setVisibility(View.VISIBLE);
            } else {
                tvXiaoyuan.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void init(View v) {

        orderDao = new OrderDao(getActivity());

        btnLogin.setOnClickListener(clickListener);
        btnLogout.setOnClickListener(clickListener);
        llMyAccount.setOnClickListener(clickListener);
        llMySt.setOnClickListener(clickListener);
        rvHead.setOnClickListener(clickListener);

        tvXiaoyuan.setVisibility(View.INVISIBLE);
        list = orderDao.findData(OrderDao.STATUS_NO);

    }

    View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnLogin:
                    openActivity(UserLoginActivity.class);
                    break;
                case R.id.btnLogout:
                    logout();
                    break;
                case R.id.rvHead:
                    if (!userBean.getIsLogin().equalsIgnoreCase("1")) {
                        openActivity(UserLoginActivity.class);
                    }
                    break;
                case R.id.llMyAccount:
                    checkStatus(SubmittedActivity.class);
                    break;
                case R.id.llMySt:
                    checkStatus(NotSubmittedActivity.class);
                    break;
            }
        }
    };

    /**
     * 判断登录状态，确定是否跳转
     *
     * @param classzz
     */
    public void checkStatus(Class classzz) {
        if (userBean.getIsLogin().equalsIgnoreCase("1")) {
            openActivity(classzz);
        } else {
            DialogHelper.showToast(getActivity(), "对不起，请先登录");
        }
    }

    /**
     * 注销用户
     */
    private void logout() {

        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.show();
        Window window = alertDialog.getWindow();
        window.setContentView(R.layout.dialog_base1);
        TextView tvTitle = (TextView) window.findViewById(R.id.tvTitle);
        Button btnLeft = (Button) window.findViewById(R.id.btnLeft);
        Button btnRight = (Button) window.findViewById(R.id.btnRight);
        tvTitle.setText("是否退出用户!");

        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserSharedPreferences.logout(getActivity());
                onResume();
                alertDialog.dismiss();
            }
        });
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }
}