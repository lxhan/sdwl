package com.richbond.sdjdsm.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.richbond.sdjdsm.entity.FragmentInfo;

import java.util.List;

/**
 * Created by liwenxun on 15/8/12.
 * 经过优化的　TabPageIndicatorFragment适配器
 */
public class OptimalTabPageIndicatorFragmentAdapter extends OptimalFragmentAdapter {

    String[] datas;

    /**
     * @param fm
     * @param frags
     */
    public OptimalTabPageIndicatorFragmentAdapter(Context context, FragmentManager fm, List<FragmentInfo> frags, String[] titles) {
        super(context, fm, frags);
        datas = titles;
    }

    public CharSequence getPageTitle(int position) {
        return datas[position % datas.length].toUpperCase();
    }

    public void notifydatas(String[] datas) {
        this.datas = datas;
    }

}