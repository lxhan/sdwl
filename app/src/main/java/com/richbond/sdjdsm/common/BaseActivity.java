package com.richbond.sdjdsm.common;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.richbond.sdjdsm.R;

/**
 * Created by liwenxun on 15/8/12.
 */
public class BaseActivity extends BaseFragmentActivity {

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
    }

    @Override
    protected void onResume() {
        /**
         * 设置为横屏
         */
        if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        super.onResume();
    }

    /**
     * 打开新的Activity
     *
     * @param classzz
     * @param bundle
     */
    public void openActivity(Class classzz, Bundle bundle) {
        Intent intent = new Intent();
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setClass(this, classzz);
        startActivity(intent);
        overridePendingTransition(R.anim.base_push_left_in,
                R.anim.base_push_left_out);
    }
}
