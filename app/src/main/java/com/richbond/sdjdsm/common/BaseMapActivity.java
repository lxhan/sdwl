/**  
 * @Title: BaseMapActivity.java
 * @Package com.uroad.cscxy.common
 * @Description: TODO(用一句话描述该文件做什么)
 * @author oupy 
 * @date 2014-5-29 上午9:23:13
 * @version V1.0  
 */
package com.richbond.sdjdsm.common;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.LocationSource;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.MyLocationStyle;


/**
 * @author Administrator
 * 所有地图界面的父类，做些　地图的初始化工作，地图界面需继承此类
 */
public class BaseMapActivity extends BaseActivity implements LocationSource, AMapLocationListener {
    MapView aMapView = null;
    AMap aMap  = null;
    OnLocationChangedListener mListener;
    LocationManagerProxy mAMapLocationManager;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
	}

	@SuppressWarnings("unchecked")
	protected void initMap(MapView mapView,Bundle arg0) {

        aMapView = mapView;
        aMapView.onCreate(arg0);
        if (aMap == null) {
            aMap = mapView.getMap();
            LatLng marker1 = new LatLng(27.548474, 109.982242);
            aMap.moveCamera(CameraUpdateFactory.changeLatLng(marker1));
            aMap.moveCamera(CameraUpdateFactory.zoomTo(12));
        }
	}


	@Override
	protected void onPause() {

		super.onPause();
        aMapView.onPause();
        deactivate();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        aMapView.onDestroy();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        aMapView.onSaveInstanceState(outState);
    }
	//使用世纪高通地图定位，但是因为这种定位有些机型是定位不了的，所以一般本系统使用百度定位和世纪高通定位两种定们
	//这两种定位之前使用的是同一种坐标系 bd09ll 一般不存在偏差
	protected void startLocation(){

	}
	
	protected void stopLocation(){

	}
	
	@Override
	protected void onResume() {
//		startLocation();
		super.onResume();
        aMapView.onResume();

    }
    protected void setUpMapLocation()
    {
        // 自定义系统定位小蓝点
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        myLocationStyle.strokeColor(Color.BLACK);// 设置圆形的边框颜色
        myLocationStyle.radiusFillColor(Color.argb(100, 0, 0, 180));// 设置圆形的填充颜色
        // myLocationStyle.anchor(int,int)//设置小蓝点的锚点
        myLocationStyle.strokeWidth(1.0f);// 设置圆形的边框粗细
//        aMap.setMyLocationStyle(myLocationStyle);
        aMap.setLocationSource(this);// 设置定位监听
        aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
        aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
//         aMap.setMyLocationType();
    }


    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (mListener != null && aMapLocation != null) {
            mListener.onLocationChanged(aMapLocation);// 显示系统小蓝点
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        mListener = onLocationChangedListener;
        if (mAMapLocationManager == null) {
            mAMapLocationManager = LocationManagerProxy.getInstance(this);
			/*
			 * mAMapLocManager.setGpsEnable(false);
			 * 1.0.2版本新增方法，设置true表示混合定位中包含gps定位，false表示纯网络定位，默认是true Location
			 * API定位采用GPS和网络混合定位方式
			 * ，第一个参数是定位provider，第二个参数时间最短是2000毫秒，第三个参数距离间隔单位是米，第四个参数是定位监听者
			 */
            mAMapLocationManager.requestLocationData(
                    LocationProviderProxy.AMapNetwork, 2000, 10, this);
        }
    }

    @Override
    public void deactivate() {
        mListener = null;
        if (mAMapLocationManager != null) {
            mAMapLocationManager.removeUpdates(this);
            mAMapLocationManager.destroy();
        }
        mAMapLocationManager = null;
    }
}
