package com.richbond.sdjdsm;

import android.os.Bundle;
import android.widget.TextView;

import com.richbond.sdjdsm.common.AppConfig;
import com.richbond.sdjdsm.common.BaseActivity;
import com.richbond.sdjdsm.util.SystemUtil;

import butterknife.Bind;
import butterknife.ButterKnife;


/*
 * 关于我们界面
 */

public class AboutActivity extends BaseActivity {
    /**
     * app版本号
     */
    @Bind(R.id.tv_version)
    public TextView tv_version;
    /**
     * 更新时间
     */
    @Bind(R.id.tv_update_time)
    public TextView tv_update_time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBaseContentLayout(R.layout.activity_aboutmjt);
        ButterKnife.bind(this);
        init();
    }
    private void init() {
        setTitle(getString(R.string.about_us));
        tv_version.setText(SystemUtil.getVersionName(this));
        tv_update_time.setText(AppConfig.UPDATE_TIME);
    }
}
